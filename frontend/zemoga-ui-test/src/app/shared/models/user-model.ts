export class UserModel {
    id: number;
    user: string;
    pass: string;
    age: number;
    status_civil: string;
    votos: number;
}
