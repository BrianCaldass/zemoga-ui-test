export class VotesItemModel {
    nombre: string;
    date: string;
    textInfo: string;
    likes: number;
    dislikes: number;
    image: string;
}
