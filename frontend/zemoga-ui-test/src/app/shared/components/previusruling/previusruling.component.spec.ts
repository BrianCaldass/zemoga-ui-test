import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PreviusrulingComponent } from './previusruling.component';

describe('PreviusrulingComponent', () => {
  let component: PreviusrulingComponent;
  let fixture: ComponentFixture<PreviusrulingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PreviusrulingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PreviusrulingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
