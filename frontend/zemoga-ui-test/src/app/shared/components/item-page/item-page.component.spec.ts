import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ItemPageComponent } from './item-page.component';
import { VotesItemModel } from '../../models/votes-item.model';
import { UserModel } from '../../models/user-model';

const mockItemsData: VotesItemModel =
{
  nombre: 'Malala Yousafzai',
  date: '1 month ago in Entertaiment',
  textInfo: 'Vestibulum diam ante, portitor a odio eget, rhoncus neque, Aenea eu vliet libero',
  likes: 27,
  dislikes: 53,
  image: 'assets/img/item4.png'
};

const mockUser: UserModel =
{
  id: 2,
  user: 'brian2',
  pass: '12345',
  age: 25,
  status_civil: 'single',
  votos: 0
};



describe('ItemPageComponent', () => {
  let component: ItemPageComponent;
  let fixture: ComponentFixture<ItemPageComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ItemPageComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ItemPageComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.infoItemVote = mockItemsData;
    expect(component).toBeTruthy();
  });

  describe('When vote is called', () => {
    it('When vote is called without user and click a button vote', () => {
      component.user = null;
      expect(component.vote(true)).toBeFalsy();
    });

    it('When vote is called and likeType is like ', () => {
      spyOn(component, 'calculateLikes').and.callThrough();
      component.user = mockUser;
      component.likeType = 'like';
      component.vote();
      expect(component.calculateLikes).toHaveBeenCalled();
      expect(component.voteAgain).toBeTruthy();
    });

    it('When vote is called and likeType is dislike ', () => {
      spyOn(component, 'calculatedisLikes').and.callThrough();
      component.user = mockUser;
      component.likeType = 'dislike';
      component.vote();
      expect(component.calculatedisLikes).toHaveBeenCalled();
      expect(component.voteAgain).toBeTruthy();
    });

    it('When vote is called and likeType is init ', () => {
      spyOn(component, 'calculatedisLikes').and.callThrough();
      spyOn(component, 'calculateLikes').and.callThrough();
      component.user = mockUser;
      component.likeType = 'init';
      component.vote();
      expect(component.calculatedisLikes).toHaveBeenCalled();
      expect(component.calculateLikes).toHaveBeenCalled();
    });

    it('When vote is called and likeType is init and button vote is true ', () => {
      component.user = mockUser;
      component.likeType = 'init';
      component.vote(true);
      expect(component.vote(true)).toBeFalsy();
    });

  });


});
