import { Component, OnInit, Input, DoCheck } from '@angular/core';
import { VotesItemModel } from './../../models/votes-item.model';
import { DomSanitizer } from '@angular/platform-browser';
import { LocalserviceService } from '../../services/localservice.service';
import { UserModel } from '../../models/user-model';
import { MessageserviceService } from './../../message/messageservice.service';

@Component({
  selector: 'app-item-page',
  templateUrl: './item-page.component.html',
  styleUrls: ['./item-page.component.scss']
})
export class ItemPageComponent implements OnInit, DoCheck {

  public user: UserModel;
  public likes: number;
  public dislikes: number;
  public likesPercentage: number;
  public dislikesPercentage: number;
  public likeType: string;
  public voteAgain: boolean;

  @Input()  infoItemVote = new VotesItemModel();
  @Input()  index: number;

  constructor(private sanitizer: DomSanitizer,
              private messageService: MessageserviceService,
              private localService: LocalserviceService) {
  }
  ngOnInit() {
    this.likes = this.infoItemVote.likes;
    this.dislikes = this.infoItemVote.dislikes;
    this.likeType = 'init';
    this.vote();
  }
  ngDoCheck(): void {
    this.user = this.localService.getUser();
  }


  public vote(voteButton?: boolean) {
    if (voteButton && this.user == null) {
      swal.fire({
        title: '<b>ADVERTENCIA</b>',
        type:  'warning',
        html:  '<br><p>To vote you must be registered</p>',
        showCloseButton: true,
        focusConfirm: false,
        confirmButtonText:
        'Ok'
      });
      return false;
    }
    if (this.likeType === 'like') {
     this.calculateLikes(1);
     this.voteAgain = true;
    } else if (this.likeType === 'dislike') {
      this.calculatedisLikes(1);
      this.voteAgain = true;
    } else if (this.likeType === 'init') {
      this.calculatedisLikes(0);
      this.calculateLikes(0);
    }
    if ( voteButton &&  this.likeType === 'init') {
      this.messageService.showWarnig('You must select a like or dislike vote');
      return false;
    }
  }

  public calculateLikes(vote: number) {
    this.likes += vote;
    const totalPercentage =  this.likes + this.dislikes;
    this.likesPercentage = Math.ceil((this.likes *  100) / totalPercentage);
    this.dislikesPercentage = Math.ceil((this.dislikes *  100) / totalPercentage);
    this.localService.updateVotes(this.index, this.likes, this.dislikes);
  }

  public calculatedisLikes(vote: number) {
    this.dislikes += vote;
    const totalPercentage =  this.likes + this.dislikes;
    this.dislikesPercentage = Math.ceil((this.dislikes *  100) / totalPercentage);
    this.likesPercentage = Math.ceil((this.likes *  100) / totalPercentage);
    this.localService.updateVotes(this.index, this.likes, this.dislikes);
  }

  public backgroundImage(image) {
    return this.sanitizer.bypassSecurityTrustStyle(`background-image: url('${image}')`);
  }

  public percentageStyle(value: number) {
    return this.sanitizer.bypassSecurityTrustStyle(`width: ${value}%`);
  }
}
