import { Component, OnInit, OnChanges, DoCheck } from '@angular/core';
import { LocalserviceService } from '../../services/localservice.service';
import { UserModel } from '../../models/user-model';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit, DoCheck {

  constructor(private localService: LocalserviceService) { }
  public user: UserModel;
  ngOnInit() {
  }
  ngDoCheck(): void {
    this.user = this.localService.getUser();
  }
  public logout() {
    sessionStorage.clear();
  }

}
