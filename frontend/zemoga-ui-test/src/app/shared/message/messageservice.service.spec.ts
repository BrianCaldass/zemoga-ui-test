import { TestBed } from '@angular/core/testing';

import { MessageserviceService } from './messageservice.service';

describe('MessageserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MessageserviceService = TestBed.get(MessageserviceService);
    expect(service).toBeTruthy();
  });

  it('When showSuccess is called ', () => {
    const service: MessageserviceService = TestBed.get(MessageserviceService);
    spyOn(service, 'showSuccess').and.callThrough();
    service.showSuccess('message');
    expect(service.showSuccess).toHaveBeenCalled();
  });

});
