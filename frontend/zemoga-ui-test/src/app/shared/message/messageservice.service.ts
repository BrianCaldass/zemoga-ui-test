import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class MessageserviceService {

  constructor() { }
  public showWarnig(message) {
    swal.fire({
      title: '<b>ADVERTENCIA</b>',
      type:  'warning',
      html:  `<br><p>${message}</p>`,
      showCloseButton: true,
      focusConfirm: false,
      confirmButtonText:
      'Ok'
    });
  }

  public showSuccess(message) {
    swal.fire({
      title: '<b>ADVERTENCIA</b>',
      type:  'success',
      html:  `<br><p>${message}</p>`,
      showCloseButton: true,
      focusConfirm: false,
      confirmButtonText:
      'Ok'
    });
  }
}
