import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from './../../../environments/environment';
import { Observable } from 'rxjs';
@Injectable({
  providedIn: 'root'
})
export class AuthserviceService {
  public apiUrl = environment.apiUrl;
  constructor(private http: HttpClient) {}

  /**
   * @author Brian Caldas
   * @description Método de login
   */
  public login(params): Observable<any> {
    return this.http.post(this.apiUrl + 'login', params);
  }

  /**
   * @author Brian Caldas
   * @description Método de Registro
   */
  public register(params): Observable<any> {
    return this.http.post(this.apiUrl + 'register', params);
  }
}
