import { TestBed } from '@angular/core/testing';
import { LocalserviceService } from './localservice.service';
const token = 'eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6MiwidXNlciI6ImJyaWFuMiIsInBhc3MiOiIxMjM0NSIsImFnZSI6MjUsInN0YXR1c19jaXZpbCI6InNpbmdsZSIsInZvdG9zIjowLCJpYXQiOjE1OTU4Njc5NzQsImV4cCI6MTU5NjA0MDc3NH0.7wjSZKiThYbeBOKLOtY9A1JQpeDabEHvH2_GjXfJrjQ';
const user = {"id":2,"user":"brian2","pass":"12345","age":25,"status_civil":"single","votos":0};
const data  = [
  {
      "nombre": "Kanye West",
      "date": "1 month ago in Entertaiment",
      "textInfo": "Vestibulum diam ante, portitor a odio eget, rhoncus neque, Aenea eu vliet libero",
      "likes": 20,
      "dislikes": 50,
      "image": "assets/img/item1.png"
  },
  {
      "nombre": "Mark Zukerberg",
      "date": "1 month ago in Business",
      "textInfo": "Thank you for voting",
      "likes": 51,
      "dislikes": 50,
      "image": "assets/img/item2.png"
  },
  {
      "nombre": "Cristina Fernández de Kirchner",
      "date": "1 month ago in Business",
      "textInfo": "Vestibulum diam ante, portitor a odio eget, rhoncus neque, Aenea eu vliet libero",
      "likes": 20,
      "dislikes": 50,
      "image": "assets/img/item3.png"
  },
  {
      "nombre": "Malala Yousafzai",
      "date": "1 month ago in Entertaiment",
      "textInfo": "Vestibulum diam ante, portitor a odio eget, rhoncus neque, Aenea eu vliet libero",
      "likes": 20,
      "dislikes": 50,
      "image": "assets/img/item4.png"
  }
];
describe('LocalserviceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: LocalserviceService = TestBed.get(LocalserviceService);
    expect(service).toBeTruthy();
  });

  it('should be created', () => {
    const service: LocalserviceService = TestBed.get(LocalserviceService);
    expect(service).toBeTruthy();
  });

  it('When updateVotes is called  with user', () => {
    const service: LocalserviceService = TestBed.get(LocalserviceService);
    spyOn(service, 'updateVotes').and.callThrough();
    service.saveData(data);
    service.updateVotes(0, 48, 54);
    expect(service.updateVotes).toHaveBeenCalled();
  });

  it('When saveUser is called', () => {
    const service: LocalserviceService = TestBed.get(LocalserviceService);
    spyOn(service, 'saveUser').and.callThrough();
    service.saveUser(token, user);
    expect(service.saveUser).toHaveBeenCalled();
  });

});
