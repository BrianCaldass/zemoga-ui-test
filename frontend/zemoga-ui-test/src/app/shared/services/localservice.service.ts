import { Injectable } from '@angular/core';
import { VotesItemModel } from '../models/votes-item.model';
import { UserModel } from '../models/user-model';

@Injectable({
  providedIn: 'root'
})
export class LocalserviceService {

  constructor() { }

  public saveUser(token, user): void {
    sessionStorage.setItem('token', JSON.stringify(token));
    sessionStorage.setItem('user', JSON.stringify(user));
  }

  public getUser(): UserModel {
    return JSON.parse(sessionStorage.getItem('user'));
  }

  public saveData(data) {
    localStorage.setItem('dataVotes', JSON.stringify(data));
  }

  public dataVotes() {
    const dataVotes = localStorage.getItem('dataVotes');
    return JSON.parse(dataVotes);
  }

  public updateVotes(index, likes, dislikes) {
    let dataVotes = new Array<VotesItemModel>();
    dataVotes = JSON.parse(localStorage.getItem('dataVotes'));
    if ( dataVotes && dataVotes.length > 0  && dataVotes[index] ) {
      dataVotes[index].likes = likes;
      dataVotes[index].dislikes =  dislikes;
    }
    this.saveData(dataVotes);
  }

}
