import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HowworksComponent } from './howworks.component';
import { SubtitlePageComponent } from '../shared/components/subtitle-page/subtitle-page.component';
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { PreviusrulingComponent } from '../shared/components/previusruling/previusruling.component';
import { ItemPageComponent } from '../shared/components/item-page/item-page.component';
import { FooterComponent } from '../shared/components/footer/footer.component';
import { HomeComponent } from '../home/home.component';
import { ItemViewComponent } from '../item-view/item-view.component';
import { LoginComponentRegistro } from '../login/login-registro.component';
import { PasttrialsComponent } from '../pasttrials/pasttrials.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';

describe('HowworksComponent', () => {
  let component: HowworksComponent;
  let fixture: ComponentFixture<HowworksComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        HowworksComponent,
        HeaderComponent,
        ItemPageComponent,
        FooterComponent,
        SubtitlePageComponent,
        HomeComponent,
        ItemViewComponent,
        LoginComponentRegistro,
        LoaderComponent,
        PreviusrulingComponent,
        PasttrialsComponent,
        HowworksComponent
       ],
       imports: [
         HttpClientTestingModule,
         RouterTestingModule,
         FormsModule
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HowworksComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
