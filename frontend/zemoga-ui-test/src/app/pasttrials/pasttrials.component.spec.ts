import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PasttrialsComponent } from './pasttrials.component';
import { SubtitlePageComponent } from '../shared/components/subtitle-page/subtitle-page.component';
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { PreviusrulingComponent } from '../shared/components/previusruling/previusruling.component';
import { ItemPageComponent } from '../shared/components/item-page/item-page.component';
import { FooterComponent } from '../shared/components/footer/footer.component';
import { ItemViewComponent } from '../item-view/item-view.component';
import { LoginComponentRegistro } from '../login/login-registro.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { HowworksComponent } from '../howworks/howworks.component';
import { HomeComponent } from '../home/home.component';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { RouterModule } from '@angular/router';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule } from '@angular/forms';


describe('PasttrialsComponent', () => {
  let component: PasttrialsComponent;
  let fixture: ComponentFixture<PasttrialsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ 
        PasttrialsComponent,
        HomeComponent,
        HeaderComponent,
        ItemPageComponent,
        FooterComponent,
        SubtitlePageComponent,
        HomeComponent,
        ItemViewComponent,
        LoginComponentRegistro,
        LoaderComponent,
        PreviusrulingComponent,
        HowworksComponent
       ],
       imports: [
         HttpClientTestingModule,
         RouterTestingModule,
         FormsModule
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PasttrialsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
