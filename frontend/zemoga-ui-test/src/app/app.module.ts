import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { ItemPageComponent } from './shared/components/item-page/item-page.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { SubtitlePageComponent } from './shared/components/subtitle-page/subtitle-page.component';
import { HomeComponent } from './home/home.component';
import { ItemViewComponent } from './item-view/item-view.component';
import { LoginComponentRegistro } from './login/login-registro.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { PreviusrulingComponent } from './shared/components/previusruling/previusruling.component';
import { PasttrialsComponent } from './pasttrials/pasttrials.component';
import { HowworksComponent } from './howworks/howworks.component';
import { LocalserviceService } from './shared/services/localservice.service';
import { AuthserviceService } from './shared/services/authservice.service';
import { FormsModule } from '@angular/forms';
import { MessageserviceService } from './shared/message/messageservice.service';

@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    ItemPageComponent,
    FooterComponent,
    SubtitlePageComponent,
    HomeComponent,
    ItemViewComponent,
    LoginComponentRegistro,
    LoaderComponent,
    PreviusrulingComponent,
    PasttrialsComponent,
    HowworksComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule
  ],
  providers: [
    LocalserviceService,
    AuthserviceService,
    MessageserviceService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
