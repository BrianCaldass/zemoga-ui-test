import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { LoginComponentRegistro } from './login/login-registro.component';
import { ItemViewComponent } from './item-view/item-view.component';
import { PasttrialsComponent } from './pasttrials/pasttrials.component';
import { HowworksComponent } from './howworks/howworks.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent
  },
  {
    path: 'login_signup/:id',
    component: LoginComponentRegistro
  },
  {
    path: 'login_signup',
    component: LoginComponentRegistro
  },
  {
    path: 'pasttrials',
    component: PasttrialsComponent
  },
  {
    path: 'howworks',
    component: HowworksComponent
  },
  {
    path: 'itemview',
    component: ItemViewComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
exports: [RouterModule]
})
export class AppRoutingModule { }
