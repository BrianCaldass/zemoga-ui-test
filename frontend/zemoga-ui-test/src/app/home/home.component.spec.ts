import { async, ComponentFixture, TestBed, fakeAsync, tick } from '@angular/core/testing';
import { HomeComponent } from './home.component';
import { SubtitlePageComponent } from '../shared/components/subtitle-page/subtitle-page.component';
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { PreviusrulingComponent } from '../shared/components/previusruling/previusruling.component';
import { ItemPageComponent } from '../shared/components/item-page/item-page.component';
import { FooterComponent } from '../shared/components/footer/footer.component';
import { ItemViewComponent } from '../item-view/item-view.component';
import { LoginComponentRegistro } from '../login/login-registro.component';
import { PasttrialsComponent } from '../pasttrials/pasttrials.component';
import { HeaderComponent } from '../shared/components/header/header.component';
import { HowworksComponent } from '../howworks/howworks.component';
import { RouterTestingModule } from '@angular/router/testing';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

describe('HomeComponent', () => {
  let component: HomeComponent;
  let fixture: ComponentFixture<HomeComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [
        HomeComponent,
        HeaderComponent,
        ItemPageComponent,
        FooterComponent,
        SubtitlePageComponent,
        HomeComponent,
        ItemViewComponent,
        LoginComponentRegistro,
        LoaderComponent,
        PreviusrulingComponent,
        PasttrialsComponent,
        HowworksComponent
       ],
       imports:[
         RouterTestingModule,
         FormsModule,
         ReactiveFormsModule
       ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HomeComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    component.statusLoader = false;
    expect(component).toBeTruthy();
  });

  describe('When loadData is called', () => {
    
    it('When loadData is called with data votes ', () => {
      spyOn(component, 'loadData').and.callThrough();
      component.loadData();
      expect(component.itemVotes.length).toBeGreaterThan(0);
    });

    it('When loadData is called without data votes ',  fakeAsync(() => {
      spyOn(component, 'loadData').and.callThrough();
      localStorage.removeItem('dataVotes');
      component.loadData();
      tick(2500);
      expect(component.statusLoader).toBeFalsy();
      expect(component.itemVotes.length).toBeGreaterThan(0);
    }));

  });



});
