import { Component, OnInit } from '@angular/core';
import { VotesItemModel } from './../shared/models/votes-item.model';
import * as data from './../shared/data/items-home.json';
import { LocalserviceService } from '../shared/services/localservice.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {
  public statusLoader: boolean;
  itemVotes = new Array<VotesItemModel>();
  constructor(private localService: LocalserviceService) {
  }

  ngOnInit() {
    this.loadData();
  }

  public loadData() {
    const dataVotes = this.localService.dataVotes();
    if ( dataVotes != null ) {
      this.itemVotes = dataVotes;
    } else {
      this.statusLoader = true;
      setTimeout(() => {
        this.itemVotes = [...data.default];
        this.statusLoader = false;
        this.localService.saveData(data.default);
      }, 2500);
    }
  }

}
