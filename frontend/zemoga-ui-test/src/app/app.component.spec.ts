import { TestBed, async } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { HeaderComponent } from './shared/components/header/header.component';
import { SubtitlePageComponent } from './shared/components/subtitle-page/subtitle-page.component';
import { LoaderComponent } from './shared/components/loader/loader.component';
import { PreviusrulingComponent } from './shared/components/previusruling/previusruling.component';
import { ItemPageComponent } from './shared/components/item-page/item-page.component';
import { FooterComponent } from './shared/components/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { ItemViewComponent } from './item-view/item-view.component';
import { LoginComponentRegistro } from './login/login-registro.component';
import { PasttrialsComponent } from './pasttrials/pasttrials.component';
import { HowworksComponent } from './howworks/howworks.component';
import { HttpClientModule } from '@angular/common/http';
import { LocalserviceService } from './shared/services/localservice.service';
import { AuthserviceService } from './shared/services/authservice.service';
import { FormsModule } from '@angular/forms';
import { BehaviorSubject } from 'rxjs';
import { RouterEvent, NavigationEnd } from '@angular/router';

describe('AppComponent', () => {
  const routerEvent$ = new BehaviorSubject<RouterEvent>(null);
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [
        RouterTestingModule,
        HttpClientModule,
        FormsModule
      ],
      declarations: [
        AppComponent,
        HeaderComponent,
        ItemPageComponent,
        FooterComponent,
        SubtitlePageComponent,
        HomeComponent,
        ItemViewComponent,
        LoginComponentRegistro,
        LoaderComponent,
        PreviusrulingComponent,
        PasttrialsComponent,
        HowworksComponent
      ],
      providers:[
        LocalserviceService,
        AuthserviceService
      ]
    }).compileComponents();
  }));

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'zemoga-ui-test'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    expect(app.title).toEqual('zemoga-ui-test');
  });

  it("Router event suscribe to login_signup", () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.debugElement.componentInstance;
    routerEvent$.next(new NavigationEnd( 1, '/', '/'));
    app.ngOnInit();
    expect(app.headerShow).toBeTruthy();
  });
});
