import { Component, OnInit } from '@angular/core';
import * as AOS from 'aos';
import { Router, NavigationEnd } from '@angular/router';
import {filter} from 'rxjs/operators';
@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  public title = 'zemoga-ui-test';
  public currentRoute: string;
  public headerShow = true;
  constructor(private route: Router) {
    this.route.events.pipe(
      filter(event => event instanceof NavigationEnd)
    ).subscribe(
      (response: any) => {
        if (response.url.includes('login_signup')) {
          this.headerShow = false;
        } else {
          this.headerShow = true;
        }
      }
    );
  }
  ngOnInit(): void {
    AOS.init({startEvent: 'load'});
  }
}
