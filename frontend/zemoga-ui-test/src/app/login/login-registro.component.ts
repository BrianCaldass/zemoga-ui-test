import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthserviceService } from '../shared/services/authservice.service';
import { LocalserviceService } from '../shared/services/localservice.service';
import { MessageserviceService } from '../shared/message/messageservice.service';

@Component({
  selector: 'app-login',
  templateUrl: './login-registro.component.html',
  styleUrls: ['./login-registro.component.scss']
})
export class LoginComponentRegistro implements OnInit {

  public loader: boolean;

  // Login
  public id: string;
  public user: string;
  public pass: string;

  // Registro
  public userRegister: string;
  public passRegister: string;
  public age: number;
  public status: string;



  constructor(private activateRouter: ActivatedRoute,
              private router: Router,
              private messageService: MessageserviceService,
              public authService: AuthserviceService,
              private localService: LocalserviceService
              ) {
    this.activateRouter.params.subscribe(
      response => {
        this.id = response.id;
      }
    );
  }

  ngOnInit() {
  }


  public login() {
    this.loader = true;
    const data = { user: this.user, pass: this.pass };
    this.authService.login(data).subscribe(
      response => {
        this.localService.saveUser(response.token, response.user);
        this.router.navigate(['/']);
        this.loader = false;
      },
      error => {
        this.messageService.showWarnig(error.error.message);
        this.loader = false;
      }
    );
  }

  public register() {
    this.loader = true;
    const data = {
      user: this.userRegister,
      pass: this.passRegister,
      age: this.age,
      status_civil: this.status
    };
    this.authService.register(data).subscribe(
      response => {
        this.messageService.showWarnig('User successfully created, log in');
        this.router.navigate(['/login_signup/login']);
        this.loader = false;
      },
      error => {
        this.messageService.showWarnig(error.error.message);
        this.loader = false;
      }
    );
  }

}
