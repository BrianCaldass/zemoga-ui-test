import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LoginComponentRegistro } from './login-registro.component';
import { FormsModule } from '@angular/forms';
import { LoaderComponent } from '../shared/components/loader/loader.component';
import { RouterTestingModule } from '@angular/router/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { throwError, of } from 'rxjs';

describe('LoginComponent', () => {
  let component: LoginComponentRegistro;
  let fixture: ComponentFixture<LoginComponentRegistro>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LoginComponentRegistro, LoaderComponent ],
      imports:[
        FormsModule,
        RouterTestingModule,
        HttpClientTestingModule
      ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LoginComponentRegistro);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });

  describe('When login is called', () => {
    it('When login is called with errors', () => {
      spyOn(component.authService, 'login').and.returnValue(throwError({error: 'error'}));
      component.user = 'brian';
      component.pass = '12345';
      component.login();
      expect(component.loader).toBeFalsy();
    });
    it('When login is called success', () => {
      const responseUserMock = {
        token: 'xxxxxxxxxxxxxxxxxxxxx',
        user: {id: 1, user: 'brian', pass: '12345', age: '27', status_civil: 'soltero', votos: 0}
      };
      spyOn(component.authService, 'login').and.returnValue(of(responseUserMock));
      component.user = 'brian';
      component.pass = '12345';
      component.login();
      expect(component.loader).toBeFalsy();
    });
  });


  describe('When register is called', () => {
    it('When register is called with errors', () => {
      spyOn(component.authService, 'register').and.returnValue(throwError({error: 'error'}));
      component.userRegister = 'brian';
      component.passRegister = '12345';
      component.age = 25;
      component.status = 'Married';
      component.register();
      expect(component.loader).toBeFalsy();
    });
    it('When register is called success', () => {
      const registerMessage = {
        message: 'User created successfully',
        user: {id: 1, user: 'brian', pass: '12345', age: '27', status_civil: 'soltero', votos: 0}
      }
      spyOn(component.authService, 'register').and.returnValue(of(registerMessage));
      component.userRegister = 'brian';
      component.passRegister = '12345';
      component.age = 25;
      component.status = 'Married';
      component.register();
      expect(component.loader).toBeFalsy();
    });
  });



});
