# README #

Prueba Zemoga Frontend y Backend nodejs

### What is this repository for? ###

* Desarrollo de la prueba de ingreso ui test para Zemoga, los requirimientos del frontend están desarrollados
al 100%, el servidor node al 50% con login persistiendo la información en un JSON y retornando un token y el registro de usuarios persistiendolos en el archivo json para validar en el frontend que solo los usuarios registrados puedan votar.
* Frontend en angular 8.2
* Servidor Node básico con express 4.17
* Version 1.0

### How do I get set up? ###

* Instalar los paquetes necesarios para cada proyecto:  npm install
* Verificar que el servidor este en el puerto 4000
* Iniciar el frontend en angular 8.2 en la carpeta frontend/zemoga-ui-test con el comando:  npm start
* Iniciar el servidor node con el comando: nodemon index.js
* La covertura de las pruebas unitarias están al 95% correr las pruebas Unitarias Del frontend con el comando: ng test --code-coverage 
