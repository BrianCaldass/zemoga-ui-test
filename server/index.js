const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');
const app = express();
const { PORT } = require('./config');
const { UserRoutes } = require('./routes');

//Body parser 
app.use(bodyParser.urlencoded({extended:true}));
app.use(bodyParser.json());
app.use(cors({ origin: true, credentials:true}));
// app.use('/', HomeRoutes);
// app.use('/', QuotesRoutes);

app.use('/', UserRoutes);


app.listen(PORT, () => {
    console.log(`Servidor en el puertox ${PORT}`);
});