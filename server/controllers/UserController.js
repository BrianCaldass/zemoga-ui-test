var jwt = require('jsonwebtoken');
const fs = require('fs');
const dataUser = require("../data/data.user");
const config = require('../config');
class UserController {

    async login(req, res) {
        const { body } = req;
        try {
            let userAuth = null;
            dataUser.map((user) => {
                if( body.user === user.user  &&  body.pass === user.pass ){
                    console.log('entro');
                    userAuth = user;
                }
            })
            if(userAuth != null) {
                const token = jwt.sign(userAuth, config.PRIVATEKEY, {expiresIn:'2d'});
                return res.status(200).send({
                    token,
                    user: userAuth
                });
            } else{
                return res.status(401).send({
                    message:'The user is not authorized or you must register'
                });
            } 
        } catch (error) {
            console.log(error);
             return res.status(500).send({
                message: 'An error has occurred creating the user',
                error
            });
            
        }
        
       
    }

    async register(req, res) {
        const { body } = req;
        try {
            let error = false;
             dataUser.map((user) => {
                if( user.user === body.user ){
                  error = true;
                }
            })
            if( error ) {
                return res.status(409).send({
                    message:'This username already exists'
                });
            }
            if( body.user != null &&  body.pass != null && body.age != null && body.status_civil != null ){
                const newUser = {
                    "id": dataUser.length+1,
                    "user": body.user,
                    "pass":  body.pass,
                    "age":  body.age,
                    "status_civil": body.status_civil,
                    "votos": 0
                }
                dataUser.push(newUser)
                const json = JSON.stringify(dataUser);
                fs.writeFile('data/data.user.json', json, 'utf8', () => {
                    return res.status(200).send({
                        message: ' User created successfully',
                        user:newUser
                    });
                });  
            }else{
                return res.status(400).send({
                    message: 'Todos los campos son requeridos'
                });   
            }
            
        } catch (error) {
            return res.status(500).send({
                message: 'Ha ocurrido un error creando el usuario',
                error
            });  
        }
        
        
    }

}

module.exports = new UserController();